# Daily Dose of Bildung
(Anastasios, Lena, Sophie für JH in München 2022)


## Idee:
Jugendliche lernen immer weniger über die Kultur dieser Welt, denn immerhin muss man in das Museum gehen und vielleicht gibt es nicht ein Museum mit dem gewünschten Thema vor Ort

-> YouTube schauen oder Minecraft spielen >> in ein Museum gehen 

## Projekt:
Darum bauen wir eine kleine Website, um jeden Tag User:innen einen Fakt/ein Zitat zu Themen wie der Mathematik, Kunst oder auch Geschichte zu zeigen.

## Technologien
- HTML, CSS
- PHP, Apache für Backend-Frontend-Verbindung aus MariaDB.
