## 1:
> text: Der goldene Schnitt bezeichnet das Teilen von Strecken in zwei Teile, wobei das Verhältnis von der längere Strecke zu der kürzeren und das von der Gesamten zur lüngeren Teilstrecke sich gleich verhält. Diese kommt in der Natur und auch in der Kunst vor, siehe der "Mona Lisa". 

> author: Wikipedia-User:innen

> quelle: https://de.wikipedia.org/wiki/Goldener_Schnitt#Goldene_Spirale

## 2:
> text: Bertrand Arthur Russell war ein britischer Mathematiker, Philosoph, Religionskritiker. Er war es unter anderem, der die Mathematik in seinem Teilwerk 'Principia Mathematica' definierte, eine der wichtigsten Werke der Mathematik.

> author: Wikipedia-User:innen

> quelle: https://de.wikipedia.org/wiki/Bertrand_Russell#Principia_Mathematica

## 3:

> text: Das Abgussmuseum in München sorgt für das Erhalten von Statuen und Figuren durch dir Abgüsse aus Gips. Auf diese Weise retten sie die Zeitzeugen der Geschichte.

> author: Anastasios Arvanidis

> quelle: https://www.abgussmuseum.de/de/abguesse-als-letze-zeugen


## 4:

> text: Das Clay Mathematics Institute in Cambridge stellte im Jahr 2000 7 ungelöste Probleme auf. Der:die Mathematiker:in, die eine Lösung zu dem Problem fand, wird mit einer Millionen US-Dollar belohnt. Ein Mathematiker löste das Problem um die Poincare-Vermutung zwei Jahre später, lehnte aber die Prämie ab.

> author: Anastasios Arvanidis

> quelle: https://www.claymath.org/millennium-problems

# 5:

> text: Ada Lovelace (1815 - 1852) war die erste Programmiererin der Welt, indem sie mit der von Charles Babbage entwickelten "Analytical Machine" ein Programm zu dem Berechnen der Bernoulli-Zahlen entwickelte.
> author: Wikipedia-User:innen
> quelle: https://de.wikipedia.org/wiki/Ada_Lovelace